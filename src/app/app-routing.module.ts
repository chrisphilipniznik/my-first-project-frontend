import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {PhotoListComponent} from './components/photo-list/photo-list.component';
import {SidePanelComponent} from './components/side-panel/side-panel.component';
import {RegisterComponent} from './components/reigster/reigster.component';
import {LoginComponent} from './components/login/login.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'photo-list', component: PhotoListComponent},
  { path: 'side-panel', component: SidePanelComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
